﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetMQ.Sockets;
using NetMQ;

namespace STUCaptureApp
{
    class MQManager
    {

        private readonly RequestSocket client = new RequestSocket();
        private bool isConnected = false;
        public int MqPort { get; set; } = 5555;
        public string MqHost { get; set; } = "localhost";
        public string MqUrl { get { return string.Format("tcp://{0}:{1}", MqHost, MqPort); } }

        public void Connect()
        {
            try
            {
                client.Connect(MqUrl);
                isConnected = true;
                Console.WriteLine("MQ Connected on TCP: {0} port {1}.", MqHost, MqPort);
            }
            catch (NetMQ.NetMQException)
            {
                throw new MQException("Unable to connect to MQ Server. Check if acquisition helper is running.");
            }

        }

        public void Disconnect()
        {
            if (isConnected)
            {
                client.Disconnect(MqUrl);
                Console.WriteLine("MQ Disconnected from TCP: {0} port {1}.", MqHost, MqPort);
                isConnected = false;
            }
        }

        public void SendFrame(String data)
        {
            if (isConnected)
            {
                try
                {
                    Console.WriteLine("Sending Frame: {0}", data);
                    client.SendFrame(data);
                    if (client.TryReceiveFrameString(new TimeSpan(0, 0, 2), out string msg))
                    {
                        Console.WriteLine("Received From AQ-Helper: {0}", msg);
                    }
                }
                catch (NetMQ.NetMQException)
                {
                    Console.WriteLine("Unable to send MQ Frame:" + data);
                }
            }
        }

        public Thread CreateThread(String data)
        {
            Thread thread = new Thread(() => SendFrame(data))
            {
                Name = "MQ-Thread"
            };
            return thread;
        }

        public void RunFrameThread(String data)
        {
            CreateThread(data).Start();
        }

    }

    public class MQException : Exception
    {
        public MQException(string message) : base(message)
        {
        }
    }

}
