﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using wgssSTU;

namespace STUCaptureApp
{
    public partial class Form1 : Form
    {
        private MQManager mqManager;
        private string outputFilename;
        private string exerciseName;
       
        public Form1(string outputFilename, string exerciseName = "Exercise", int mqServerPort = 0, String mqServerHost = null)
        {
            this.outputFilename = outputFilename;
            this.exerciseName = exerciseName;
            InitializeComponent();

            // Connect MQ broker 
            if (mqServerPort > 0)
            {
                mqManager = new MQManager
                {
                    MqPort = mqServerPort
                };

                if (mqServerHost != null)
                {
                    mqManager.MqHost = mqServerHost;
                }


                try
                {
                    mqManager.Connect();
                }
                catch (MQException exc)
                {
                    MessageBox.Show("Error: " + exc.Message, "MQ Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }

        }


        private void Form1_Load(object sender, EventArgs e)
        {
            CaptureHandler handler;
            StreamWriter streamWriter;

            UsbDevices usbDevices = new wgssSTU.UsbDevices();

            if (usbDevices.Count == 0)
            {
                MessageBox.Show("No Wacom tablet is connected.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Application.Exit();
            }
            else
            {
                IUsbDevice usbDevice = usbDevices[0]; // select a device
                Debug.WriteLine("DEVICE Selected");

                try
                {
                    streamWriter = new StreamWriter(outputFilename, false);
                }
                catch (System.IO.IOException)
                {
                    MessageBox.Show("Unable to open open file for writing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Application.Exit();
                    return;
                }

                Tablet m_tablet = new wgssSTU.Tablet();
                //wgssSTU.ICapability m_capability = m_tablet.getCapability();
                //wgssSTU.IInformation m_information = m_tablet.getInformation();
                IErrorCode ec = m_tablet.usbConnect(usbDevice, true);


                if (ec.value != 0)
                {
                    Debug.WriteLine("Error occured.");
                    return;
                }

                ICapability m_capability = m_tablet.getCapability();
                IInformation m_information = m_tablet.getInformation();



                //StreamWriter streamWriter = new StreamWriter(outputFilename, false);
                handler = new CaptureHandler(m_tablet, m_capability, m_information, RecordingStarted);
                handler.TransferBitmapToTablet(handler.CreateExerciseUI(exerciseName, "Please redraw given image on the sign pad surface."));
                handler.EnableInkingMode();
                FormClosing += delegate
                {
                    Debug.WriteLine("ENDING");
                    handler.CleanScreen();
                    handler.EnableInkingMode(false);
                    handler.SaveCapturedData(streamWriter);
                    streamWriter.Close();
                };

                lb_readyCapture.Visible = true;

                // Send MQ ready message
                if (mqManager != null)
                {
                    mqManager.RunFrameThread("HandAQUS ready");
                }


            }
        }

        private void RecordingStarted()
        {
            lb_recording.Visible = true;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
