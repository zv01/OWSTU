﻿namespace STUCaptureApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_readyCapture = new System.Windows.Forms.Label();
            this.lb_recording = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lb_readyCapture
            // 
            this.lb_readyCapture.AutoSize = true;
            this.lb_readyCapture.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_readyCapture.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lb_readyCapture.Location = new System.Drawing.Point(70, 34);
            this.lb_readyCapture.Name = "lb_readyCapture";
            this.lb_readyCapture.Size = new System.Drawing.Size(217, 25);
            this.lb_readyCapture.TabIndex = 0;
            this.lb_readyCapture.Text = "Ready to capture data...";
            this.lb_readyCapture.Visible = false;
            // 
            // lb_recording
            // 
            this.lb_recording.AutoSize = true;
            this.lb_recording.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_recording.ForeColor = System.Drawing.Color.Red;
            this.lb_recording.Location = new System.Drawing.Point(102, 84);
            this.lb_recording.Name = "lb_recording";
            this.lb_recording.Size = new System.Drawing.Size(157, 25);
            this.lb_recording.TabIndex = 1;
            this.lb_recording.Text = "Recording data";
            this.lb_recording.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(355, 170);
            this.Controls.Add(this.lb_recording);
            this.Controls.Add(this.lb_readyCapture);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "STUCapture";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lb_readyCapture;
        private System.Windows.Forms.Label lb_recording;
    }
}

