﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;

namespace STUCaptureApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(String[] args)
        {
            // Parse ommand line arguments
            // Example: --mqport=500 --mqhost=localhost 'C:\DATA\protocol.svc' 
            int mqPort = 0;
            String mqHost = null;
            string outputFile = null;
            string exerciseName = null;

            Regex mqPortExpression = new Regex(@"--mqport=(?<port>\d+)");
            Regex mqHostExpression = new Regex(@"--mqhost=(?<host>\S+)");

            foreach (var it in args.Select((x, i) => new { Value = x, Index = i }))
            {

                if (it.Value == "--help")
                {
                    Console.WriteLine("Usage: STUCaptureApp.exe [--mqport=5555] [--mqhost=localhost] [exercise name] [output file]");
                    return;
                }

                // Port --mqport=<port> argument
                Match portMatch = mqPortExpression.Match(it.Value);
                if (portMatch.Success)
                {
                    mqPort = Convert.ToInt32(portMatch.Groups["port"].Value);
                    continue;
                }

                // Port --mqhost=<host> argument
                Match hostMatch = mqHostExpression.Match(it.Value);
                if (hostMatch.Success)
                {
                    mqHost = hostMatch.Groups["host"].Value;
                    continue;
                }

                if (it.Index == args.Length - 2)  // last argument should be exercise name
                {
                    exerciseName = it.Value;
                }

                    if (it.Index == args.Length - 1)  // last argument should be file name
                {
                    if (!Directory.Exists(Path.GetDirectoryName(it.Value)))
                    {
                        MessageBox.Show("Given output file path " + it.Value + " is not valid. Check if the directory exists and its writable.", "Argument error",
                            MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        try
                        {
                            Path.GetFullPath(it.Value);
                            outputFile = it.Value;
                        }
                        catch (ArgumentException)
                        {
                            MessageBox.Show("Given output path " + it.Value + " is not valid. Please avoid using special characters in the path.", "Argument error",
                           MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }


                    }

                }
            }


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(outputFile, exerciseName, mqPort, mqHost));
        }
    }
}
