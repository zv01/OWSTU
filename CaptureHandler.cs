﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wgssSTU;
using System.Diagnostics;
using System.Drawing;
using System.IO;

namespace STUCaptureApp
{
    class CaptureHandler
    {

        private Tablet tablet;
        private ICapability capability;
        private IInformation information;

        private Stopwatch excStopwatch = new Stopwatch();

        private bool firstTouch = false;

        private Bitmap uiBitmap;
        private Action firstTouchUIAction;

        // Keep captured data as packet List for saving
        private List<Tuple<long, IPenData>> sessionData = new List<Tuple<long, IPenData>>();


        public Bitmap CreateExerciseUI(String exerciseName, String description)
        {
            Bitmap bitmap = new Bitmap(capability.screenWidth, capability.screenHeight,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Graphics gfx = Graphics.FromImage(bitmap);
            gfx.Clear(Color.White);

            Font headingFont = new Font(FontFamily.GenericSansSerif, 25);
            Font descriptionFont = new Font(FontFamily.GenericSansSerif, 13, FontStyle.Italic);
            Brush blackBrush = new SolidBrush(Color.Black);
            Pen pen = new Pen(blackBrush);

            gfx.DrawString(exerciseName, headingFont, blackBrush, new Point(5, 5));
            if (description != "")
            {
                gfx.DrawString(description, descriptionFont, blackBrush, new Point(14, 40));
            }

            Brush redBrush = new SolidBrush(Color.Red);
            gfx.DrawEllipse(new Pen(redBrush, 15), 760, 15, 15, 15);

            gfx.DrawLine(new Pen(blackBrush, 3), new Point(10, 420), new Point(780, 420));
            this.uiBitmap = bitmap;
            return bitmap;

        }

        public void CleanScreen()
        {
            Bitmap bitmap = new Bitmap(capability.screenWidth, capability.screenHeight,
    System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Graphics gfx = Graphics.FromImage(bitmap);
            gfx.Clear(Color.Red);
            TransferBitmapToTablet(bitmap);
        }

        public void TransferBitmapToTablet(Bitmap bitmap)
        {

            // Calculate the encodingMode that will be used to update the image
            wgssSTU.ProtocolHelper protocolHelper = new wgssSTU.ProtocolHelper();
            ushort idP = tablet.getProductId();
            encodingFlag encodingFlag = (wgssSTU.encodingFlag)protocolHelper.simulateEncodingFlag(idP, 0);
            encodingMode encodingMode;


            if ((encodingFlag & wgssSTU.encodingFlag.EncodingFlag_24bit) != 0)
            {
                encodingMode = tablet.supportsWrite() ? encodingMode.EncodingMode_24bit_Bulk : encodingMode.EncodingMode_24bit;
            }
            else if ((encodingFlag & wgssSTU.encodingFlag.EncodingFlag_16bit) != 0)
            {
                encodingMode = tablet.supportsWrite() ? encodingMode.EncodingMode_16bit_Bulk : encodingMode.EncodingMode_16bit;
            }
            else
            {
                // assumes 1bit is available
                encodingMode = encodingMode.EncodingMode_1bit;
            }

            System.IO.MemoryStream stream = new MemoryStream();
            bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
            byte[] bitmapData = (byte[])protocolHelper.resizeAndFlatten(stream.ToArray(),
            0, 0, (uint)bitmap.Width, (uint)bitmap.Height, capability.screenWidth, capability.screenHeight,
            (byte)encodingMode, wgssSTU.Scale.Scale_Fit, 0, 0);

            tablet.writeImage((byte)encodingMode, bitmapData);

        }



        public CaptureHandler(Tablet tablet, ICapability capability, IInformation information, Action firstTouchCallback)
        {
            this.tablet = tablet;
            this.capability = capability;
            this.information = information;
            this.firstTouchUIAction = firstTouchCallback;

            Debug.WriteLine("Handler started connected: {0}, size: {1}x{2}", information.modelName, capability.screenWidth, capability.screenHeight);


            // register handler
            tablet.onPenData += new ITabletEvents2_onPenDataEventHandler(onPenData);

        }

        // Remove first captured in-air data 
        private void RemoveFirstInAirData(List<Tuple<long, IPenData>> penDataList)
        {
            int CountToFisrtOne = 0;

            for (int i = 0; i < penDataList.Count; i++)
            {
                int inAir = Convert.ToInt32(penDataList[i].Item2.sw);
                if (inAir == 1)
                {
                    CountToFisrtOne = i;
                    break;
                }
            }
            penDataList.RemoveRange(0, CountToFisrtOne);
        }

        private void RemoveLastInAirData(List<Tuple<long, IPenData>> penDataList)
        {
            int firstlastZeroPacketIndex = 0;
            int CountToTheEnd = 0;

            for (int i = penDataList.Count - 1; i > 0; i--)
            {
                bool inAir = System.Convert.ToBoolean(penDataList[i].Item2.sw);
                if (!inAir)
                {
                    firstlastZeroPacketIndex = i;
                    CountToTheEnd = penDataList.Count - i;
                    break;
                }
            }
            penDataList.RemoveRange(firstlastZeroPacketIndex, CountToTheEnd);
        }


        private void onPenData(IPenData penData)
        {
            if (penData.rdy)
            {
                Debug.WriteLine("X {0} Y {1} PR {2}", (float)penData.x, (float)penData.y, (float)penData.pressure);
                if(!firstTouch && Convert.ToBoolean(penData.sw))
                {
                    firstTouch = true;
                    Debug.WriteLine("First touch");
                    excStopwatch.Start();
                    firstTouchUIAction();

                }
                if (firstTouch)
                {
                    sessionData.Add(new Tuple<long, IPenData>(excStopwatch.ElapsedMilliseconds, penData));
                }
                //DrawPoint((int)penData.x, (int)penData.y);
            }
        }

        public void EnableInkingMode(bool enable = true)
        {
            if(enable)
            {
                tablet.setInkingMode(0x01);
            }
            else
            {
                tablet.setInkingMode(0x00);
            }
            
        }

        public void SaveCapturedData(StreamWriter sw)
        {
            RemoveFirstInAirData(sessionData);
            RemoveLastInAirData(sessionData);

            // First line is number of samples (one number)
            sw.WriteLine(string.Format("{0}", sessionData.Count));

            foreach(Tuple<long, IPenData> d in sessionData)
            {
                IPenData pd = d.Item2;
                sw.WriteLine("{0} {1} {2} {3}", d.Item1, pd.x, pd.y, pd.pressure);
            }

            // Close stream writer
            sw.Flush();
            sw.Close();
        }

    }
}
